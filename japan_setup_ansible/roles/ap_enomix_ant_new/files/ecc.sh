#!/bin/sh

export MTALK_ROOT=/mtalk
export MTALK_HOME=$MTALK_ROOT/mtalk510
export LINE_HOME=$MTALK_ROOT/line-bot
export _JAVA_ROOT=$MTALK_ROOT/_JAVA
export JAVA_HOME=$_JAVA_ROOT/jdk1.8.0_212
export ANT_HOME=$_JAVA_ROOT/apache-ant-1.8.4
export APACHE_HOME=$_JAVA_ROOT/apache-2.4.41

export PATH=.:$JAVA_HOME/bin:$ANT_HOME/bin:$PATH

ENOMIX_HOME=`cd ../;pwd`
ENOMIX_VERSION=5.1.3

case "`uname`" in
CYGWIN*) 
  ENOMIX_HOME=`cygpath --absolute --windows "$ENOMIX_HOME"`
  ;;
esac

TYPE=$2

ENOMIX_LOG=$ENOMIX_HOME/logs

if [ ! -d "$ENOMIX_LOG" ]; then
  mkdir $ENOMIX_LOG
fi

cd ..

RETVAL=0

case "$TYPE" in
  gateway)
        JETTY_HOST=
        START_PORT=19010
        STOP_PORT=19019
        STOP_KEY=stop_gateway
        CONTEXT_PATH=/gateway
        BASE_RESOURCE=webapps/gateway
        SSL_CONF_FILE=
        ;;
  router)
        JETTY_HOST=
        START_PORT=19020
        STOP_PORT=19029
        STOP_KEY=stop_router
        CONTEXT_PATH=/router
        BASE_RESOURCE=webapps/router
        SSL_CONF_FILE=
        ;;
  engine)
        JETTY_HOST=
        START_PORT=19030
        STOP_PORT=19039
        STOP_KEY=stop_engine
        CONTEXT_PATH=/engine
        BASE_RESOURCE=webapps/engine
        SSL_CONF_FILE=
        ;;
  rtis)
        JETTY_HOST=
        START_PORT=19040
        STOP_PORT=19049
        STOP_KEY=stop_rtis
        CONTEXT_PATH=/rtis
        BASE_RESOURCE=webapps/rtis
        SSL_CONF_FILE=
        ;;
  webapps)
        JETTY_HOST=
        START_PORT=19090
        STOP_PORT=19099
        STOP_KEY=stop_webapps
        CONTEXT_PATH=/enomix
        BASE_RESOURCE=webapps/webapps
        SSL_CONF_FILE=
        ;;
  restapi)
        JETTY_HOST=
        START_PORT=17060
        STOP_PORT=17069
        STOP_KEY=stop_restapi
        CONTEXT_PATH=/restapi
        BASE_RESOURCE=webapps/restapi
        SSL_CONF_FILE=
        ;;
  webroot)
        JETTY_HOST=
        START_PORT=17070
        STOP_PORT=17079
        STOP_KEY=stop_webroot
        CONTEXT_PATH=/
        BASE_RESOURCE=webapps/webroot
        SSL_CONF_FILE=
        ;;
  thirdparty)
        JETTY_HOST=
        START_PORT=19070
        STOP_PORT=19079
        STOP_KEY=stop_thirdparty
        CONTEXT_PATH=/thirdparty
        BASE_RESOURCE=webapps/thirdparty
        SSL_CONF_FILE=
        ;;
  legw)
        JETTY_HOST=
        START_PORT=19050
        STOP_PORT=19059
        STOP_KEY=stop_legw
        CONTEXT_PATH=/legw
        BASE_RESOURCE=webapps/legw
        SSL_CONF_FILE=
        ;;
  *)
        echo $"Usage: $0 {run|stop} {router|gateway|engine|rtis|webapps|restapi|webroot|thirdparty|legw}"
        RETVAL=2
        ;;
esac


if [ "$1" = "run" ]; then
  JVM_OPTS="-DENOMIX_HOME=$ENOMIX_HOME -DENOMIX_LOG_HOME=$ENOMIX_LOG -Dorg.owasp.esapi.resources=$ENOMIX_HOME/conf -Dresource.minify=true -Djetty.home=$ENOMIX_HOME -Djdk.tls.ephemeralDHKeySize=2048 -XX:MaxPermSize=256m -XX:PermSize=128m -Xms128m -Xmx512m"
  JVM_OPTS="$JVM_OPTS -DSTOP.PORT=$STOP_PORT -DSTOP.KEY=$STOP_KEY -Dfile.encoding=utf-8"
  if [ "$JETTY_HOST" != "" ]; then
    JVM_OPTS="$JVM_OPTS -Dspectra.jetty.server.properties=host:$JETTY_HOST"
  fi
  if [ "$SSL_CONF_FILE" != "" ]; then
    JVM_OPTS="$JVM_OPTS -Dssl.conf.file=$SSL_CONF_FILE"
  fi

  if [ "$SSL_PORT" = "" ]; then
    nohup java $JVM_OPTS -jar lib/ecc-99-release-$ENOMIX_VERSION.jar DEBUG=true type=$TYPE webPort=$START_PORT contextPath=$CONTEXT_PATH baseResource=$BASE_RESOURCE > $ENOMIX_LOG/${TYPE}.stdout.log 2>&1 &
  else 
    nohup java $JVM_OPTS -jar lib/ecc-99-release-$ENOMIX_VERSION.jar DEBUG=true type=$TYPE webPort=$START_PORT contextPath=$CONTEXT_PATH baseResource=$BASE_RESOURCE sslPort=$SSL_PORT keystore=$KEY_STORE keyPassword=$KEY_PASSWORD > $ENOMIX_LOG/${TYPE}.stdout.log 2>&1 & 
  fi

  RETVAL=$?
elif [ "$1" = "stop" ]; then
  java -DSTOP.PORT=$STOP_PORT -DSTOP.KEY=$STOP_KEY -jar lib/ecc-99-release-$ENOMIX_VERSION.jar --stop
fi
exit $RETVAL

#!/bin/sh

if [ ${USER} != `stat -c "%U" ${0##*/}` ]; then
  echo "Operation is not permitted."
  exit 0
fi

wait_process()
{
  local NAME="$1"
  local FILE="$2"
  local STR="$3"

  until [ -e $FILE ]
  do
    sleep 1;
  done

  printf "%-20s" "starting $NAME"

  LINE=0
  while true
  do
    line=`wc -l $FILE | sed 's/^ *//g' | cut -d " " -f 1`
    [ "X" != "X`tail -n +$LINE $FILE | grep "$STR"`" ] && break;
    LINE=$line
    sleep 1;
  done

  printf "%20s\n" "......... [OK]"
}

# remove all stdout log
rm ../logs/*.stdout.log

#cd ../apps/solr
#./run.sh > /dev/null
#echo "starting solr             ......... [OK]"
#cd ../../bin

./ecc.sh run router
wait_process "router" "../logs/router.stdout.log" "jetty started"

./ecc.sh run gateway
wait_process "gateway" "../logs/gateway.stdout.log" "jetty started"

./ecc.sh run engine
wait_process "engine" "../logs/engine.stdout.log" "jetty started"

#su - root -c $MTALK_HOME'/apps/rvts/bin/rvts.sh run > /dev/null'
#echo "starting rvts             ......... [OK]"

./ecc.sh run rtis
wait_process "rtis" "../logs/rtis.stdout.log" "jetty started"

./ecc.sh run webapps
wait_process "webapps" "../logs/webapps.stdout.log" "jetty started"

./ecc.sh run restapi
wait_process "restapi" "../logs/restapi.stdout.log" "jetty started"

./ecc.sh run webroot
wait_process "webroot" "../logs/webroot.stdout.log" "jetty started"

./scheduler_run.sh > /dev/null
echo "starting scheduler        ......... [OK]"

./status.sh

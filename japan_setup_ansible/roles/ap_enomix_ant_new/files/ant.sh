#!/bin/sh
export MTALK_ROOT=/mtalk
export MTALK_HOME=$MTALK_ROOT/mtalk510
export LINE_HOME=$MTALK_ROOT/line-bot
export _JAVA_ROOT=$MTALK_ROOT/_JAVA
export JAVA_HOME=$_JAVA_ROOT/jdk1.8.0_212
export ANT_HOME=$_JAVA_ROOT/apache-ant-1.8.4
export APACHE_HOME=$_JAVA_ROOT/apache-2.4.41

export PATH=.:$JAVA_HOME/bin:$ANT_HOME/bin:$PATH

ant

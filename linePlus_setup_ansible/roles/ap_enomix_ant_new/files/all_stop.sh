#!/bin/sh

if [ ${USER} != `stat -c "%U" ${0##*/}` ]; then
  echo "Operation is not permitted."
  exit 0
fi

#cd ../apps/rvts/bin
#./rvts.sh stop > /dev/null
#echo "stoping rvts             ......... [OK]"
#cd ../../../bin
#sleep 2

./scheduler_stop.sh
echo "stoping scheduler        ......... [OK]"
sleep 1

#cd ../apps/solr
#./stop.sh > /dev/null
#echo "stoping solr             ......... [OK]"
#cd ../../bin
#sleep 2

./ecc.sh stop webroot
echo "stoping webroot          ......... [OK]"
sleep 1

./ecc.sh stop restapi
echo "stoping restapi          ......... [OK]"
sleep 2

./ecc.sh stop webapps
echo "stoping webapps          ......... [OK]"
sleep 1

./ecc.sh stop rtis
echo "stoping rtis             ......... [OK]"
sleep 2

./ecc.sh stop engine
echo "stoping engine           ......... [OK]"
sleep 1

./ecc.sh stop gateway
echo "stoping gateway          ......... [OK]"
sleep 2

./ecc.sh stop router
echo "stoping router           ......... [OK]"
sleep 2

./status.sh

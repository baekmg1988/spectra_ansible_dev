#!/bin/sh

export MTALK_ROOT=/mtalk
export MTALK_HOME=$MTALK_ROOT/mtalk510
export LINE_HOME=$MTALK_ROOT/line-bot
export _JAVA_ROOT=$MTALK_ROOT/_JAVA
export JAVA_HOME=$_JAVA_ROOT/jdk1.8.0_212
export ANT_HOME=$_JAVA_ROOT/apache-ant-1.8.4
export APACHE_HOME=$_JAVA_ROOT/apache-2.4.41

export PATH=.:$JAVA_HOME/bin:$ANT_HOME/bin:$PATH

ENOMIX_HOME=`cd ..;pwd`
ENOMIX_LOG_HOME=$ENOMIX_HOME/logs

for i in "$ENOMIX_HOME"/webapps/scheduler/bin/*.jar; do
  CLASSPATH="$CLASSPATH":"$i"
done

for i in "$ENOMIX_HOME"/webapps/scheduler/lib/*.jar; do
  CLASSPATH="$CLASSPATH":"$i"
done

PROCESS="ps -ef | grep ENOMIX_HOME | grep '$ENOMIX_HOME/' | grep scheduler | grep -v 'grep'"
proc=`eval $PROCESS`
pid=`echo $proc | awk '{print $2}'`

if [ -n "$pid" ]; then
    echo $pid > spectraScheduler.pid
    echo "server($pid) already running"
else
    nohup java -DScheduler -Xms128m -Xmx256m -DENOMIX_HOME=$ENOMIX_HOME -DENOMIX_LOG_HOME=$ENOMIX_LOG_HOME -Dlogback.configurationFile=$ENOMIX_HOME/conf/logback-scheduler.xml -classpath $CLASSPATH spectra.ee.scheduler.scheduler.service.Scheduler > $ENOMIX_HOME/logs/scheduler.stdout.log 2>&1 &
    echo $! > spectraScheduler.pid
    echo "server started"
fi
